---
theme: midnight
toc: true
style: style.css
---

# WebR + Observable Framework = 🤗

[Live](https://hrbrmstr.app/webr-framework/) • [Source](https://codeberg.org/hrbrmstr/webr-framework)

---

DuckDB's unique extensions to SQL make operations in it very similar to what R's {tidyverse} set out to do. However, it is still SQL, and lots of folks have gotten lazy in SQL thanks to {tidyverse}-esque idioms that are present in multiple programming language ecosystems.

So, it makes sense to use WebR in a Framework context, especially since we seem to be fine with the DuckDB WASM load-time penalty (though WebR's is, and always will be, much noticeably larger). In fact, we loaded WebR while you were reading these two paragraphs, so, perhaps the load-time thing isn't all that meaningful in a dashboard/report context anyway?

But, I digress.

We're here to see how to use WebR in a Framework context, so let's get on with the show.

## Loading WebR

Since my last post on WebR, George & team have been making massive enhancements that level-up WebR's capabilities, and make it easier to work with. For instance, I've shunted the WebR init code into an `R.js` file in `components/`. The most essential parts of that are:

````R
let { WebR } = await import("https://webr.r-wasm.org/latest/webr.mjs");

export let webR = new WebR();

await webR.init();
await webR.installPackages([ "ggplot2", "dplyr", "tidyr" ]);
await webR.evalR(`
library(ggplot2)
library(dplyr)
library(tidyr)
`);
````

First note that we're importing the latest and greatest version of WebR. That's a bit dangerous, but you can pin them at specific versions. Just use `v0.2.2` (etc.) instead of `latest`. Also note that I've not told you to configure certain web server headers. Unless you need certain, somewhat esoteric functionality, you can use WebR without being a super nerd expert in web hosting configurations.

We're installing some {tidyverse} packages (we don't need all of them, this time), and loading all of them up so we can use the functions and object within those packages whenever we need.

This is also a good place to put lots of other things you will need across _dashport_ pages.

## Data Frames Got An Upgrade!

Thanks to some new helper functions, R's data frames are much easier to work with in WebR.

Let's wire up {ggplot2}'s `economics` data frame to an Observable `Input` `table`:

````md
```js
const economics = (await webR.evalR(`economics`)).toD3()
view(Inputs.table(economics))
```
````

```js
const economics = (await webR.evalR(`economics`)).toD3()
view(Inputs.table(economics))
```

WebR's helper function `toD3` makes working with data frames from R a 🍰.

Similarly, we can pass in JavaScript "data frames" (arrays of named objects) to R and they "just work". Observable ships with some built-in objects, so let's pass the `penguins` data frame to R and work with it:

````md
```js
const sel = view(Inputs.select(_.uniq(penguins.map(d => d.species)), { label: "Species" }))
```
```js
const rPenguins = await webR.evalR(`
penguins |>
  filter(
    species == sel
  )
`, { env: { penguins: penguins, sel: sel }})
```
```js
view(Inputs.table(await (rPenguins.toD3())))
```
````

```js
const sel = view(Inputs.select(_.uniq(penguins.map(d => d.species)), { label: "Species" }))
```
```js
const rPenguins = await webR.evalR(`
penguins |>
  filter(
    species == sel
  )
`, { env: { penguins: penguins, sel: sel }})
```
```js
view(Inputs.table(await (rPenguins.toD3())))
```

That's right. R's {dplyr} is doing all the heavy lifting here, thanks to WebR being smart enough to convert `penguins` to an R data frame via the `env:` argument.

I kind of really dislike using window functions in SQL, even with some of DuckDB's niceties. I find something like this much cleaner:

````md
```js
const speciesSexPerIsland = await webR.evalR(`
penguins |> 
  count(island, species, sex, sort=TRUE)
`, { env: { penguins: penguins }})
```
```js
view(Inputs.table((await speciesSexPerIsland.toD3())))
```
````

```js
const speciesSexPerIsland = await webR.evalR(`
penguins |> 
  count(island, species, sex, sort=TRUE)
`, { env: { penguins: penguins }})
```
```js
view(Inputs.table((await speciesSexPerIsland.toD3())))
```

## ggPlot2

Observable `Plot` is a joy to use since it has a similar feel to the venerable {ggplot2}. But, let's face it, {ggplot2} reigns supreme in the hearts of many a data-scientist, myself included. Thankfully, George & team made working with R graphics output even easier in a WASM context.

I snuck a very lightweight {hrbrthemes} theme into `R.js`, so let's see how my boring, standard {mtcars} plot works in a Framework context.

````md
```js
const res = await webR.globalShelter.captureR(`
# BORING ggplot2 code
`);
const image = res.images[0];
const canvas = document.getElementById("plot");
const ctx = context2d(image.width, image.height, 1); # see R.js
ctx.drawImage(image, 0, 0, image.width, image.height);
ctx.canvas.style.width = "100%";
```
````

```js
 const res = await webR.globalShelter.captureR(`
print(
  ggplot() +
    geom_point(
      data = mtcars,
      aes(wt, mpg),
      color = "white"
    ) +
    labs(
      x = "Weight (tonnes)", y = "Fuel Efficiency (MPG)",
      title = "The Usual {hrbrthemes} Demo Plot Title",
      subtitle = "The Usual {hrbrthemes} Demo Plot Subtitle",
      caption = "The Usual {hrbrthemes} Demo Plot Caption"
    ) +
    theme_ipsum_rc(grid="XY") +
    theme(
      text = element_text(color = "white"),
      title = element_text(color = "white"),
      axis.text = element_text(color = "white"),
      panel.background = element_rect(fill = NA, color = NA)
    )
)
`);
  const image = res.images[0];
  const canvas = document.getElementById("plot");
  const ctx = context2d(image.width, image.height, 1);
  ctx.drawImage(image, 0, 0, image.width, image.height);
  ctx.canvas.style.width = "100%";
```

```js
ctx.canvas
```

Fonts work! And, go ahead…resize the window! It "just works"!

We can make these plots a bit more interactive, too. (I'm going to ask you to view the source for this since it's a bit too long for an embed…)

```js
const color = view(Inputs.select(x11colors, {value: "steelblue", label: "Favorite color" }));
```

<div class="grid grid-cols-2">
<div class="card">

```js
ctx2.canvas
```

</div>

<div class="card">

```js
ctx3.canvas
```

</div>
</div>

## The Need For Abstraction

I think for WebR in regular JavaScript-land (which includes Framework) to become more of "a thing", we're going to need some extra abstraction layer to make some of these operations easier to incorporate. I'm not sure I trust any individual (including myself) to do that. And, I'm also not sure I want a corporate entity to do it, since it'll inevitably involve some either direct service lock-in, or alot of cruft to support corporate hosted services. (I _may_ be nudging rOpenSci to take a look in this direction).

There is also nothing stopping me, or any other invested WebR user from starting something. Or, at least using our own-rolled creations in our own projects.

But, before that happens, I would at least like to encourage R-capable folks to give WebR a go at wrangling data in your Framework apps; especially, if you're struggling with SQL (though you should also likely fix that bug).

## Just One More Thing

Notice the "Changelog" in the left sidebar? I've taken to using that for _dashports_ I share with or build for others so they can keep track of what's changed. 

Also, since Framework [uses `markdown-it`](https://observablehq.com/framework/config#markdownit), you can use the `footnotes` plugin to make note of important things, like events that caused "adverse data events".


```js
 const res = await webR.globalShelter.captureR(`
print(
  ggplot() +
    geom_point(
      data = mtcars,
      aes(wt, mpg),
      color = color
    ) +
    labs(
      x = "Weight (tonnes)", y = "Fuel Efficiency (MPG)",
      title = "The Usual {hrbrthemes} Demo Plot Title",
      subtitle = "The Usual {hrbrthemes} Demo Plot Subtitle",
      caption = "The Usual {hrbrthemes} Demo Plot Caption"
    ) +
    theme_ipsum_rc(grid="XY") +
    theme(
      text = element_text(color = "white"),
      title = element_text(color = "white"),
      axis.text = element_text(color = "white"),
      panel.background = element_rect(fill = NA, color = NA)
    )
)
`,
    { env: { color } }
  );
  const image = res.images[0];
  const canvas = document.getElementById("plot");
  const ctx2 = context2d(image.width, image.height, 1);
  ctx2.drawImage(image, 0, 0, image.width, image.height);
  ctx2.canvas.style.width = "100%";
```

```js
 const res = await webR.globalShelter.captureR(`
print(
  ggplot() +
    geom_point(
      data = mtcars,
      aes(wt, mpg)
    ) +
    labs(
      x = "Weight (tonnes)", y = "Fuel Efficiency (MPG)",
      title = "The Usual {hrbrthemes} Demo Plot Title",
      subtitle = "The Usual {hrbrthemes} Demo Plot Subtitle",
      caption = "The Usual {hrbrthemes} Demo Plot Caption"
    ) +
    theme_ipsum_rc(grid="XY") +
    theme(
      text = element_text(color = "white"),
      title = element_text(color = "white"),
      axis.text = element_text(color = "white"),
      panel.background = element_rect(fill = color, color = NA)
    )
)
`,
    { env: { color } }
  );
  const image = res.images[0];
  const canvas = document.getElementById("plot");
  const ctx3 = context2d(image.width, image.height, 1);
  ctx3.drawImage(image, 0, 0, image.width, image.height);
  ctx3.canvas.style.width = "100%";
```

```js
import { webR, context2d } from './components/R.js';
```

```js
const x11colors = ["aliceblue", "antiquewhite", "aqua", "aquamarine", "azure", "beige", "bisque", "black", "blanchedalmond", "blue", "blueviolet", "brown", "burlywood", "cadetblue", "chartreuse", "chocolate", "coral", "cornflowerblue", "cornsilk", "crimson", "cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray", "darkgreen", "darkgrey", "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange", "darkorchid", "darkred", "darksalmon", "darkseagreen", "darkslateblue", "darkslategray", "darkslategrey", "darkturquoise", "darkviolet", "deeppink", "deepskyblue", "dimgray", "dimgrey", "dodgerblue", "firebrick", "floralwhite", "forestgreen", "fuchsia", "gainsboro", "ghostwhite", "gold", "goldenrod", "gray", "green", "greenyellow", "grey", "honeydew", "hotpink", "indianred", "indigo", "ivory", "khaki", "lavender", "lavenderblush", "lawngreen", "lemonchiffon", "lightblue", "lightcoral", "lightcyan", "lightgoldenrodyellow", "lightgray", "lightgreen", "lightgrey", "lightpink", "lightsalmon", "lightseagreen", "lightskyblue", "lightslategray", "lightslategrey", "lightsteelblue", "lightyellow", "lime", "limegreen", "linen", "magenta", "maroon", "mediumaquamarine", "mediumblue", "mediumorchid", "mediumpurple", "mediumseagreen", "mediumslateblue", "mediumspringgreen", "mediumturquoise", "mediumvioletred", "midnightblue", "mintcream", "mistyrose", "moccasin", "navajowhite", "navy", "oldlace", "olive", "olivedrab", "orange", "orangered", "orchid", "palegoldenrod", "palegreen", "paleturquoise", "palevioletred", "papayawhip", "peachpuff", "peru", "pink", "plum", "powderblue", "purple", "rebeccapurple", "red", "rosybrown", "royalblue", "saddlebrown", "salmon", "sandybrown", "seagreen", "seashell", "sienna", "silver", "skyblue", "slateblue", "slategray", "slategrey", "snow", "springgreen", "steelblue", "tan", "teal", "thistle", "tomato", "turquoise", "violet", "wheat", "white", "whitesmoke", "yellow", "yellowgreen"];
```

```js
// need this to ensure some libs come over thanks to tree-sitting
const junk = Plot.plot({})
```